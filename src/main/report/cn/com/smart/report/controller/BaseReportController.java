package cn.com.smart.report.controller;

import cn.com.smart.web.controller.base.BaseController;

public abstract class BaseReportController extends BaseController {

    protected static final String BASE_REPORT_VIEW_DIR = "report/";
    
}
